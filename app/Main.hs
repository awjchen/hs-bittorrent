{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Control.Monad.Combinators
import Data.Bifunctor (first)
import qualified Data.ByteString as B
import Data.ByteString (ByteString)
import Data.Char (chr, ord)
import qualified Data.Map.Strict as M
import Data.Void (Void)
import Data.Word (Word8)
import System.Environment (getArgs)
import Text.Megaparsec
import Text.Megaparsec.Byte

main :: IO ()
main = do
  [filepath] <- getArgs
  main' filepath

main' :: FilePath -> IO ()
main' filepath = do
  input <- B.readFile filepath
  let result =
        first errorBundlePretty $
          parse bencode "" input
  case result of
    Left err -> putStrLn err
    Right bencode -> print bencode

type Parser = Parsec Void ByteString

data Bencode
  = BString ByteString
  | BInteger Integer
  | BList [Bencode]
  | BDict (M.Map ByteString Bencode)
  deriving (Show)

bencode :: Parser Bencode
bencode =
  BString <$> try bString
    <|> BInteger <$> try bInteger
    <|> BList <$> try bList
    <|> BDict <$> bDict

bString :: Parser ByteString
bString = do
  len <- number
  _ <- char_ ':'
  takeP Nothing (fromIntegral len)

bInteger :: Parser Integer
bInteger = do
  char_ 'i'
  i <- optional (char_ '-') >>= \case
    Nothing -> number
    Just _ -> negate <$> positiveNumber
  char_ 'e'
  pure i

number :: Parser Integer
number = try positiveNumber <|> (0 <$ char_ '0')

positiveNumber :: Parser Integer
positiveNumber =  do
  d1 <- satisfy isNonZeroDigit
  ds <- takeWhileP Nothing isDigit
  let i = read @Integer $ map (chr . fromIntegral) (d1 : B.unpack ds)
  pure i

isDigit :: Word8 -> Bool
isDigit x = x >= 48 && x <= 57

isNonZeroDigit :: Word8 -> Bool
isNonZeroDigit x = x >= 49 && x <= 57

bList :: Parser [Bencode]
bList = do
  char_ 'l'
  manyTill bencode (char_ 'e')

bDict :: Parser (M.Map ByteString Bencode)
bDict = do
  char_ 'd'
  M.fromList <$> manyTill ((,) <$> bString <*> bencode) (char_ 'e')

char_ :: Char -> Parser Word8
char_ c = char $ fromIntegral $ ord c
